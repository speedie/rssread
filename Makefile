SHELL = /bin/sh
NAME = rssread
VER = 0.1

include options.mk

help:
	@echo "make install     Install ${NAME}."
	@echo "make uninstall   Uninstall ${NAME}."

install:
	cp ${NAME} ${PREFIX}${DESTDIR}/bin
	chmod +x ${PREFIX}${DESTDIR}/bin/${NAME}

uninstall:
	rm -rf ~/.config/${NAME}
	rm -f ${PREFIX}${DESTDIR}/bin/${NAME}

dist:
	mkdir -p ${NAME}-${VER}
	cp ${NAME} LICENSE Makefile ${NAME}-${VER}
	tar -cf ${NAME}-${VER}.tar ${NAME}-${VER}
	gzip ${NAME}-${VER}.tar
	rm -rf ${NAME}-${VER}.tar ${NAME}-${VER}
